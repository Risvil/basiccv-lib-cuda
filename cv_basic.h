/*
 * cv_basic.h
 *
 *  Created on: Feb 12, 2016
 *      Author: Ruben Avalos Elvira
 */

#ifndef CV_BASIC_H_
#define CV_BASIC_H_

#include <vector>
#include <list>
#include <queue>
#include <cstring>

#include "bitmap_image/bitmap_image.hpp"

namespace cvb
{

/**
 * Class to represent a morphological structure element (also known as kernel)
 */
class StructureElement
{
public:
	enum EElementType { SQUARE, CROSS, /*ELLIPSE,*/ CIRCLE };

	StructureElement(int size, EElementType type = SQUARE);
	~StructureElement();

	/**
	 * Returns minimum pixel value over all pixel neighbours specified at kernel
	 */
	bool MinPixelVisitor(const bitmap_image& source, int x, int y) const;

	/**
	 * Returns maximum pixel value over all pixel neighbours specified at kernel
	 */
	bool MaxPixelVisitor(const bitmap_image& source, int x, int y) const;

	/**
	 * Returns mean pixel value over all pixel neighbours specified at kernel
	 */
	int MeanPixelVisitor(const bitmap_image& source, int x, int y) const;

	/**
	 * Returns kernel size
	 */
	int GetSize() const;

	/**
	 * Prints kernel structure to standard output
	 */
	void Print() const;

private:
	/**
	 * Whether or not the specified point is too far away to be considered a valid neighbour
	 * (only useful for non-square kernels)
	 */
	bool IsPointTooFarFromRadius(int i, int j) const;

private:
	/** kernel size */
	int m_size;

	/** kernel type */
	EElementType m_type;

	/** kernel element data */
	int* m_pKernel;
};

/**
 * Represents a labelable 2D point
 */
struct Point
{
	int x;
	int y;
	int contourId;

	Point();
	Point(int _x, int _y);
	Point(int _x, int _y, int _contourId);

	bool operator==(const Point& rhs) const;
};

//typedef std::vector<int> Histogram;
typedef int Histogram[256];
typedef std::vector<Point> PointVector;
typedef std::vector<PointVector> ContourContainer;
typedef std::list<Point> PointList;


/**
 * Returns whether or not images are the same size
 */
bool AreImagesSameSize(const bitmap_image& source, const bitmap_image& dest);

/**
 * Converts source image into grayscale and stores it on dest
 */
bool ToGrayScale(const bitmap_image& source, bitmap_image& dest);

/**
 * Calculates image histogram
 */
void CalcHistogram(const bitmap_image& image, Histogram& histogram);

/**
 * Perform histogram equalization on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool HistogramEqualization(const bitmap_image& source, bitmap_image& dest);

/**
 * Performs binary threshold on source and stores it on dest
 *
 * If pixel value < threshold will become 0, 255 otherwise. Use inverted flag
 * to invert this behaviour
 *
 * Returns true if images are same size, false otherwise
 */
bool BinaryThreshold(const bitmap_image& source, bitmap_image& dest, int threshold, bool inverted=false);

/**
 * Performs image erosion on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool Erode(const bitmap_image& source, bitmap_image& dest, const StructureElement& kernel, int iterations = 1);

/**
 * Performs image dilation on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool Dilate(const bitmap_image& source, bitmap_image& dest, const StructureElement& kernel, int iterations = 1);

/**
 * Performs a median blur operation on source image, and stores it on dest
 * Kernel size (ksize) must be and odd number
 *
 * Returns true if images are same size and kernel size is valid, false otherwise
 */
bool MedianBlur(const bitmap_image& source, bitmap_image& dest, int ksize);

/**
 * Performs contour detection over source image.
 */
void DetectContours(const bitmap_image& image, ContourContainer& contours);

};
#endif /* CV_BASIC_H_ */
