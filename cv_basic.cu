/*
 * cv_basic.cpp
 *
 *  Created on: Feb 12, 2016
 *      Author: Ruben Avalos Elvira
 */
#include "cv_basic.h"
#include <algorithm>


namespace cvb
{
// ==================================================================================================
// Class StructureElement
// ==================================================================================================

StructureElement::StructureElement(int size, EElementType type)
: m_size(size),
  m_type(type)
{
	m_pKernel = new int[size*size*2];

	const int minInRange = -(size-1)/2;

	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			if (!IsPointTooFarFromRadius(i, j))
			{
				const int index = i*size*2 + j*2;

				const int y = minInRange + i;
				const int x = minInRange + j;

				m_pKernel[index] = x;
				m_pKernel[index + 1] = y;
			}
		}
	}
}

StructureElement::~StructureElement()
{
	delete[] m_pKernel;
}

/**
 * Returns minimum pixel value over all pixel neighbours specified at kernel
 */
bool StructureElement::MinPixelVisitor(const bitmap_image& source, int x, int y) const
{
	bool foregroundPixel = true;

	const std::size_t width = source.width();
	const std::size_t height = source.height();

	for (int i = 0; i < m_size; ++i)
	{
		for (int j = 0; j < m_size; ++j)
		{
			if (IsPointTooFarFromRadius(i, j))
			{
				continue;
			}

			const int index = i*m_size*2 + j*2;

			const int xOffset = m_pKernel[index];
			const int yOffset = m_pKernel[index + 1];

			unsigned int posX = x + xOffset;
			unsigned int posY = y + yOffset;

			if (posX >= width || posY >= height)
			{
				continue;
			}

			unsigned char red;
			unsigned char green;
			unsigned char blue;

			source.get_pixel(posX, posY, red, green, blue);

			// assuming binary one channel image, using red channel for calculations
			if (red < 255)
			{
				foregroundPixel = false;
			}

		}
	}

	return foregroundPixel;
}

/**
 * Returns maximum pixel value over all pixel neighbours specified at kernel
 */
bool StructureElement::MaxPixelVisitor(const bitmap_image& source, int x, int y) const
{
	bool foregroundPixel = false;

	const std::size_t width = source.width();
	const std::size_t height = source.height();

	for (int i = 0; i < m_size; ++i)
	{
		for (int j = 0; j < m_size; ++j)
		{
			if (IsPointTooFarFromRadius(i, j))
			{
				continue;
			}

			const int index = i*m_size*2 + j*2;

			const int xOffset = m_pKernel[index];
			const int yOffset = m_pKernel[index + 1];

			unsigned int posX = x + xOffset;
			unsigned int posY = y + yOffset;

			if (posX >= width || posY >= height)
			{
				continue;
			}

			unsigned char red;
			unsigned char green;
			unsigned char blue;

			source.get_pixel(posX, posY, red, green, blue);

			// assuming binary one channel image, using red channel for calculations
			if (red == 255)
			{
				foregroundPixel = true;
			}

		}
	}

	return foregroundPixel;
}

/**
 * Returns mean pixel value over all pixel neighbours specified at kernel
 */
int StructureElement::MeanPixelVisitor(const bitmap_image& source, int x, int y) const
{
	const std::size_t width = source.width();
	const std::size_t height = source.height();

	std::vector<int> neighborPixels;

	for (int i = 0; i < m_size; ++i)
	{
		for (int j = 0; j < m_size; ++j)
		{
			if (IsPointTooFarFromRadius(i, j))
			{
				continue;
			}

			const int index = i*m_size*2 + j*2;

			const int xOffset = m_pKernel[index];
			const int yOffset = m_pKernel[index + 1];

			unsigned int posX = x + xOffset;
			unsigned int posY = y + yOffset;

			if (posX >= width || posY >= height)
			{
				continue;
			}

			unsigned char red;
			unsigned char green;
			unsigned char blue;

			source.get_pixel(posX, posY, red, green, blue);

			// assuming binary one channel image, using red channel for calculations
			neighborPixels.push_back(red);
		}
	}

	int meanValue = 0;
	for (std::size_t i = 0; i < neighborPixels.size(); ++i)
	{
		meanValue += neighborPixels[i];
	}
	meanValue /= neighborPixels.size();

	return meanValue;
}

/**
 * Returns kernel size
 */
int StructureElement::GetSize() const
{
	return m_size;
}

/**
 * Prints kernel structure to standard output
 */
void StructureElement::Print() const
{
	for (int i = 0; i < m_size; ++i)
	{
		for (int j = 0; j < m_size; ++j)
		{
			if (IsPointTooFarFromRadius(i, j))
			{
				std::cout << " (N , N) ";
				continue;
			}

			const int index = i*m_size*2 + j*2;

			const int x = m_pKernel[index];
			const int y = m_pKernel[index + 1];

			std::cout << " (" << x << ", " << y << ") ";
		}
		std::cout << std::endl;
	}

	const int totalSize = m_size*m_size*2;
	std::cout << "sequential memory (total size=" << totalSize << "): " << std::endl;
	for (int i = 0; i < totalSize; ++i)
	{
		std::cout << m_pKernel[i] << ", ";
	}

	std::cout << std::endl;
}

/**
 * Whether or not the specified point is too far away to be considered a valid neighbour
 * (only useful for non-square kernels)
 */
bool StructureElement::IsPointTooFarFromRadius(int i, int j) const
{
	bool retValue = false;

	switch (m_type)
	{
		case SQUARE:
		{
			retValue = false;
			break;
		}

		case CIRCLE:
		{
			const int minInRange = -(m_size-1)/2;
			const int maxDistance = (m_size-1)/2;

			const int y = minInRange + i;
			const int x = minInRange + j;

			retValue = std::abs(x) + std::abs(y) > maxDistance;

			break;
		}

		case CROSS:
		{
			retValue = !(i == 0 || j == 0);

			break;
		}
	}

	return retValue;
}

// ==================================================================================================
// Structure Point
// ==================================================================================================

Point::Point()
: x(0),
  y(0),
  contourId(-1)
{
}

Point::Point(int _x, int _y)
: x(_x),
  y(_y),
  contourId(-1)
{
}

Point::Point(int _x, int _y, int _contourId)
: x(_x),
  y(_y),
  contourId(_contourId)
{
}

bool Point::operator==(const Point& rhs) const
{
	return x == rhs.x && y == rhs.y;
}

// ==================================================================================================

/**
 * Returns whether or not images are the same size
 */
bool AreImagesSameSize(const bitmap_image& source, const bitmap_image& dest)
{
	return source.width() == dest.width() && source.height() == dest.height();
}

__global__ void ToGrayScaleGPU(unsigned char* source, unsigned char* dest, int width, int height)
{
	int i, j, index;
	unsigned int red, green, blue, gray;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			red = source[index];
			green = source[index + 1];
			blue = source[index + 2];

			gray = 0.21*red + 0.72*green + 0.07*blue;

			dest[index] = gray;
			dest[index + 1] = gray;
			dest[index + 2] = gray;
		}
	}
}

/**
 * Converts source image into grayscale and stores it on dest
 */
bool ToGrayScale(const bitmap_image& source, bitmap_image& dest)
{
	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	// Initialize memory
	const unsigned int height = source.height();
	const unsigned int width  = source.width();
	const unsigned int size = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceSource;
	cudaMalloc((void**)&deviceSource, size);
	cudaMemcpy(deviceSource, source.c_data(), size, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, size);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	ToGrayScaleGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height);

	// Copy memory back to host
	cudaMemcpy(dest.data(), deviceDest, size, cudaMemcpyDeviceToHost);

	cudaFree(deviceSource);
	cudaFree(deviceDest);

	return true;
}

__global__ void CalcHistogramGPU(unsigned char* image, int* histogram, int width, int height)
{
	int i, j, index;
	unsigned char color;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			// assuming grayscale image, using red channel for calculations
			color = image[index];
			atomicAdd(&histogram[color], 1);
		}
	}
}

/**
 * Calculates image histogram
 */
void CalcHistogram(const bitmap_image& image, Histogram& histogram)
{
	const unsigned int height = image.height();
	const unsigned int width  = image.width();

	// Initialize device memory
	unsigned char* deviceImage;
	const unsigned int imageSize = height*width*3;
	cudaMalloc((void**)&deviceImage, sizeof(unsigned char)*imageSize);
	cudaMemcpy(deviceImage, image.c_data(), imageSize, cudaMemcpyHostToDevice);

	int* deviceHistogram;
	const unsigned int histogramSize = sizeof(int)*256;
	cudaMalloc((void**)&deviceHistogram, histogramSize);
	cudaMemset(deviceHistogram, 0, histogramSize);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	CalcHistogramGPU<<<dimGrid, dimBlock>>>(deviceImage, deviceHistogram, width, height);

	// Copy results back to host
	cudaMemcpy(histogram, deviceHistogram, histogramSize, cudaMemcpyDeviceToHost);

	// Free memory
	cudaFree(deviceImage);
	cudaFree(deviceHistogram);
}

__global__ void HistogramEqualizationGPU(unsigned char* source, unsigned char* dest, int width, int height, int* cdf, float cdf_min)
{
	int i, j, index;
	unsigned char color, normColor;

	const int size = width*height;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			color = source[index];
			normColor = ( (cdf[color] - cdf_min) / size ) * 255;

			dest[index] = normColor;
			dest[index+1] = normColor;
			dest[index+2] = normColor;
		}
	}
}

/**
 * Perform histogram equalization on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool HistogramEqualization(const bitmap_image& source, bitmap_image& dest)
{
	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	Histogram histogram;
	CalcHistogram(source, histogram);

	// Calculate cumulative distribution function
	int cdf[256];
	cdf[0] = histogram[0];
	for (int i = 1; i < 256; ++i)
	{
		cdf[i] = cdf[i-1] + histogram[i];
	}

	float cdf_min = cdf[0];

	// Apply equalization
	const unsigned int height = source.height();
	const unsigned int width  = source.width();

	// Init device memory
	unsigned char* deviceSource;
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;
	cudaMalloc((void**)&deviceSource, imageSize);
	cudaMemcpy(deviceSource, source.c_data(), imageSize, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, imageSize);

	int* deviceCDF;
	const unsigned int cdfSize = sizeof(int) * 256;
	cudaMalloc((void**)&deviceCDF, cdfSize);
	cudaMemcpy(deviceCDF, cdf, cdfSize, cudaMemcpyHostToDevice);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	HistogramEqualizationGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height, deviceCDF, cdf_min);

	// Copy results back to host
	cudaMemcpy(dest.data(), deviceDest, imageSize, cudaMemcpyDeviceToHost);

	// Free memory
	cudaFree(deviceSource);
	cudaFree(deviceDest);
	cudaFree(deviceCDF);


	return true;
}

__global__ void BinaryThresholdGPU(unsigned char* source, unsigned char* dest, int width, int height, int threshold, bool inverted)
{
	int i, j, index;
	unsigned char color, destColor;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			color = source[index];
			if (!inverted)
			{
				destColor = (color > threshold) ? 255 : 0;
			}
			else
			{
				destColor = (color > threshold) ? 0 : 255;
			}

			dest[index] = destColor;
			dest[index + 1] = destColor;
			dest[index + 2] = destColor;
		}
	}
}

/**
 * Performs binary threshold on source and stores it on dest
 *
 * If pixel value < threshold will become 0, 255 otherwise. Use inverted flag
 * to invert this behaviour
 *
 * Returns true if images are same size, false otherwise
 */
bool BinaryThreshold(const bitmap_image& source, bitmap_image& dest, int threshold, bool inverted)
{
	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	const unsigned int height = source.height();
	const unsigned int width  = source.width();

	// Init device memory
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceSource;
	cudaMalloc((void**)&deviceSource, imageSize);
	cudaMemcpy(deviceSource, source.c_data(), imageSize, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, imageSize);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	BinaryThresholdGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height, threshold, inverted);

	// Copy back memory to host
	cudaMemcpy(dest.data(), deviceDest, imageSize, cudaMemcpyDeviceToHost);

	// Free resources
	cudaFree(deviceSource);
	cudaFree(deviceDest);

	return true;
}

__device__ bool ErodeKernel(unsigned char* source, int index, int width, int height, int ksize)
{
	int color;

	// Create and apply kernel at fly
	const int minInRange = -(ksize-1)/2;

	for (int i = 0; i < ksize; ++i)
	{
		for (int j = 0; j < ksize; ++j)
		{
			const int yOffset = minInRange + i;
			const int xOffset = minInRange + j;

			const int pos = index + xOffset*width*3 + yOffset*3;

			if (pos < 0 || pos > width*height*3)
			{
				continue;
			}

			color = source[pos];

			// Has a background neighbour, so this one will become background also
			if (color < 255)
			{
				return false;
			}
		}
	}

	return true;
}

__global__ void ErodeGPU(unsigned char* source, unsigned char* dest, int width, int height, int ksize)
{
	int i, j, index;
	unsigned char color;
	bool isForegroundPixel;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			isForegroundPixel = ErodeKernel(source, index, width, height, ksize);

			color = isForegroundPixel ? 255 : 0;

			dest[index] = color;
			dest[index + 1] = color;
			dest[index + 2] = color;
		}
	}
}

/**
 * Performs image erosion on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool Erode(const bitmap_image& source, bitmap_image& dest, const StructureElement& kernel, int iterations)
{
	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	const unsigned int height = source.height();
	const unsigned int width  = source.width();

	// Init device memory
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceSource;
	cudaMalloc((void**)&deviceSource, imageSize);
	cudaMemcpy(deviceSource, source.c_data(), imageSize, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, imageSize);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	bitmap_image currentSource = source;
	for (int currentIt = 0; currentIt < iterations; ++currentIt)
	{
		cudaMemcpy(deviceSource, currentSource.c_data(), imageSize, cudaMemcpyHostToDevice);

		ErodeGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height, kernel.GetSize());

		// Copy back memory to host
		cudaMemcpy(dest.data(), deviceDest, imageSize, cudaMemcpyDeviceToHost);

		currentSource = dest;
	}

	// Free resources
	cudaFree(deviceSource);
	cudaFree(deviceDest);

	return true;
}

__device__ bool DilateKernel(unsigned char* source, int index, int width, int height, int ksize)
{
	int color;

	// Create and apply kernel at fly
	const int minInRange = -(ksize-1)/2;

	for (int i = 0; i < ksize; ++i)
	{
		for (int j = 0; j < ksize; ++j)
		{
			const int yOffset = minInRange + i;
			const int xOffset = minInRange + j;

			const int pos = index + xOffset*width*3 + yOffset*3;

			if (pos < 0 || pos > width*height*3)
			{
				continue;
			}

			color = source[pos];

			// Has a foreground neighbour, so this one will also become foreground
			if (color > 0)
			{
				return true;
			}
		}
	}

	return false;
}

__global__ void DilateGPU(unsigned char* source, unsigned char* dest, int width, int height, int ksize)
{
	int i, j, index;
	unsigned char color;
	bool isForegroundPixel;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			isForegroundPixel = DilateKernel(source, index, width, height, ksize);

			color = isForegroundPixel ? 255 : 0;

			dest[index] = color;
			dest[index + 1] = color;
			dest[index + 2] = color;
		}
	}
}

/**
 * Performs image dilation on source and stores it on dest
 *
 * Returns true if images are same size, false otherwise
 */
bool Dilate(const bitmap_image& source, bitmap_image& dest, const StructureElement& kernel, int iterations)
{
	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	const unsigned int height = source.height();
	const unsigned int width  = source.width();

	// Init device memory
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceSource;
	cudaMalloc((void**)&deviceSource, imageSize);
	cudaMemcpy(deviceSource, source.c_data(), imageSize, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, imageSize);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	bitmap_image currentSource = source;
	for (int currentIt = 0; currentIt < iterations; ++currentIt)
	{
		cudaMemcpy(deviceSource, currentSource.c_data(), imageSize, cudaMemcpyHostToDevice);

		DilateGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height, kernel.GetSize());

		// Copy back memory to host
		cudaMemcpy(dest.data(), deviceDest, imageSize, cudaMemcpyDeviceToHost);

		currentSource = dest;
	}

	// Free resources
	cudaFree(deviceSource);
	cudaFree(deviceDest);

	return true;
}

__device__ int MedianKernel(unsigned char* source, int index, int width, int height, int ksize)
{
	int colorAcc = 0;
	int neighbours = 0;
	int meanColor = 0;

	// Create and apply kernel at fly
	const int minInRange = -(ksize-1)/2;

	for (int i = 0; i < ksize; ++i)
	{
		for (int j = 0; j < ksize; ++j)
		{
			const int yOffset = minInRange + i;
			const int xOffset = minInRange + j;

			const int pos = index + xOffset*width*3 + yOffset*3;

			if (pos < 0 || pos > width*height*3)
			{
				continue;
			}

			colorAcc += source[pos];
			neighbours++;
		}
	}

	if (neighbours > 0)
	{
		meanColor = colorAcc / neighbours;
	}

	return meanColor;
}

__global__ void MedianBlurGPU(unsigned char* source, unsigned char* dest, int width, int height, int ksize)
{
	int i, j, index;
	unsigned char color;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			color = MedianKernel(source, index, width, height, ksize);

			dest[index] = color;
			dest[index + 1] = color;
			dest[index + 2] = color;
		}
	}
}

/**
 * Performs a median blur operation on source image, and stores it on dest
 * Kernel size (ksize) must be and odd number
 *
 * Returns true if images are same size and kernel size is valid, false otherwise
 */
bool MedianBlur(const bitmap_image& source, bitmap_image& dest, int ksize)
{
	// Only accepting odd kernel sizes
	if (ksize % 2 != 1)
	{
		return false;
	}

	if (!AreImagesSameSize(source, dest))
	{
		return false;
	}

	const unsigned int height = source.height();
	const unsigned int width  = source.width();

	// Init device memory
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceSource;
	cudaMalloc((void**)&deviceSource, imageSize);
	cudaMemcpy(deviceSource, source.c_data(), imageSize, cudaMemcpyHostToDevice);

	unsigned char* deviceDest;
	cudaMalloc((void**)&deviceDest, imageSize);

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	MedianBlurGPU<<<dimGrid, dimBlock>>>(deviceSource, deviceDest, width, height, ksize);

	// Copy back memory to host
	cudaMemcpy(dest.data(), deviceDest, imageSize, cudaMemcpyDeviceToHost);

	// Free resources
	cudaFree(deviceSource);
	cudaFree(deviceDest);

	return true;
}

__global__ void GetForegroundPointsGPU(unsigned char* image, int* contours, int* nextContourIdx, int width, int height)
{
	int i, j, index, contourIdx;
	unsigned char color;

	for (	j = blockIdx.y * gridDim.x + blockIdx.x;
			j < width;
			j += gridDim.x
		)
	{
		for (	i = threadIdx.y * blockDim.x + threadIdx.x;
				i < height;
				i += blockDim.x
			)
		{
			index = i*width*3 + j*3;

			if (index > width*height*3 + 2)
			{
				return;
			}

			color = image[index];

			// Foreground pixel
			if (color > 0)
			{
				contourIdx = atomicAdd(nextContourIdx, 3);

				// Add point to contours
				contours[contourIdx] = j; // x
				contours[contourIdx + 1] = i; // y
				contours[contourIdx + 2] = -1; // id
			}
		}
	}
}

void DetectContoursI2(const bitmap_image& image, int width, int height, const PointVector& foregroundPoints, bool* labelMap, ContourContainer& contours)
{
	int currentLabel = 1;
	for (size_t i = 0; i < foregroundPoints.size(); ++i)
	{
		// Use stack-based flood fill algorithm to label contours
		Point p = foregroundPoints[i];

		const int mapIdx = p.x + p.y*width;

		if (labelMap[mapIdx])
		{
			continue;
		}

		std::queue<Point> pointsToProcess;
		pointsToProcess.push(p);

		PointVector contour;
		while (!pointsToProcess.empty())
		{
			Point p = pointsToProcess.front();
			pointsToProcess.pop();

			if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height)
			{
				continue;
			}

			const int mapIdx = p.x + p.y*width;

			if (!labelMap[mapIdx])
			{
				// Mark this point as labeled
				labelMap[mapIdx] = true;

				unsigned char red;
				unsigned char green;
				unsigned char blue;

				image.get_pixel(p.x, p.y, red, green, blue);

				if (red < 255)
				{
					continue;
				}

				// label point
				p.contourId = currentLabel;
				contour.push_back(p);

				// Push its 4 neighbours
				pointsToProcess.push(Point(p.x-1, p.y));
				pointsToProcess.push(Point(p.x+1, p.y));
				pointsToProcess.push(Point(p.x, p.y-1));
				pointsToProcess.push(Point(p.x, p.y+1));
			}

		}
		currentLabel++;

		if (!contour.empty())
		{
			contours.push_back(contour);
		}
	}
}

void DetectContoursII(const bitmap_image& image, int width, int height, Point p, int label, bool* labelMap, PointVector& contour);

void DetectContoursI(const bitmap_image& image, int width, int height, const PointVector& foregroundPoints, bool* labelMap, ContourContainer& contours)
{
	int currentLabel = 1;
	for (size_t i = 0; i < foregroundPoints.size(); ++i)
	{
		// Use recursive flood fill algorithm to label contours
		Point p = foregroundPoints[i];

		PointVector contour;

		DetectContoursII(image, width, height, p, currentLabel, labelMap, contour);

		currentLabel++;

		if (!contour.empty())
		{
			contours.push_back(contour);
		}
	}
}

void DetectContoursII(const bitmap_image& image, int width, int height, Point p, int label, bool* labelMap, PointVector& contour)
{
	const int mapIdx = p.x + p.y*width;

	if (labelMap[mapIdx])
	{
		return;
	}

	if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height)
	{
		return;
	}

	if (!labelMap[mapIdx])
	{
		// Mark this point as labeled
		labelMap[mapIdx] = true;

		unsigned char red;
		unsigned char green;
		unsigned char blue;

		image.get_pixel(p.x, p.y, red, green, blue);

		if (red < 255)
		{
			return;
		}

		// label point
		p.contourId = label;
		contour.push_back(p);

		// Push its 4 neighbours
		DetectContoursII(image, width, height, Point(p.x-1, p.y), label, labelMap, contour);
		DetectContoursII(image, width, height, Point(p.x+1, p.y), label, labelMap, contour);
		DetectContoursII(image, width, height, Point(p.x, p.y-1), label, labelMap, contour);
		DetectContoursII(image, width, height, Point(p.x, p.y+1), label, labelMap, contour);
	}
}

/**
 * Performs contour detection over source image.
 */
void DetectContours(const bitmap_image& image, ContourContainer& contours)
{
	const int width = image.width();
	const int height = image.height();

	// Init device memory
	const unsigned int imageSize = sizeof(unsigned char) * width * height * 3;

	unsigned char* deviceImage;
	cudaMalloc((void**)&deviceImage, imageSize);
	cudaMemcpy(deviceImage, image.c_data(), imageSize, cudaMemcpyHostToDevice);

	int* deviceContours;
	int contoursSize = sizeof(int) * width * height * 3;
	cudaMalloc((void**)&deviceContours, contoursSize);
	cudaMemset(deviceContours, 0, contoursSize);

	int* deviceNextContourIdx;
	cudaMalloc((void**)&deviceNextContourIdx, sizeof(int));
	cudaMemset(deviceNextContourIdx, 0, sizeof(int));

	// Call kernel
	int gridSize = width * 3;
	if (height > 1024)
	{
		gridSize += std::ceil(height / 1024.0f);
	}
	int blockSize = std::min((int)height, 1024);

	dim3 dimGrid(gridSize);
	dim3 dimBlock(blockSize);

	GetForegroundPointsGPU<<<dimGrid, dimBlock>>>(deviceImage, deviceContours, deviceNextContourIdx, width, height);

	int contoursBuffer[width*height*3];
	cudaMemcpy(contoursBuffer, deviceContours, contoursSize, cudaMemcpyDeviceToHost);

	int lastContourIdx;
	cudaMemcpy(&lastContourIdx, deviceNextContourIdx, sizeof(int), cudaMemcpyDeviceToHost);

	// Get foreground points from buffer data
	PointVector foregroundPoints;
	for (int i = 0; i < lastContourIdx; i += 3)
	{
		Point p;
		p.x = contoursBuffer[i];
		p.y = contoursBuffer[i + 1];
		p.contourId = contoursBuffer[i + 2];

		foregroundPoints.push_back(p);
	}

	bool labelMap[width*height];
	memset(&labelMap, 0, sizeof(bool) * width * height);

	DetectContoursI(image, width, height, foregroundPoints, labelMap, contours);


}

};

