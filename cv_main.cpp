#include <iostream>
#include "cv_basic.h"

#define BENCHMARKING 0

#if BENCHMARKING
#include <sys/time.h>

// Performance metrics variables
struct timeval start, end;
long mtime, seconds, useconds;

void StartBenchmark()
{
	// Performance metrics start
	gettimeofday(&start, NULL);
}
void EndBenchmark()
{
	// Performance metrics end
	gettimeofday(&end, NULL);

	seconds = end.tv_sec - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;

	mtime = (seconds * 1000 + useconds / 1000.0) + 0.5;
	std::cout << "Total time: " << mtime << " milliseconds." << std::endl;
}

#else

void StartBenchmark() {}
void EndBenchmark() {}

#endif

using namespace cvb;


int LowImageProcessing()
{
	std::string imageName = "monedas_low.bmp";

	bitmap_image image(imageName);

	if (!image)
	{
		std::cout << "Error - Failed to open: input.bmp" << std::endl;
		return 1;
	}

	const unsigned int height = image.height();
	const unsigned int width  = image.width();

	bitmap_image gray(width, height);
	ToGrayScale(image, gray);
	gray.save_image("out_gray.bmp");

	bitmap_image equalized(width, height);
	HistogramEqualization(gray, equalized);
	equalized.save_image("out_equalized.bmp");

	bitmap_image blurred(width, height);
	MedianBlur(equalized, blurred, 5);
	blurred.save_image("out_blurred.bmp");

	bitmap_image thresholded(width, height);
	BinaryThreshold(blurred, thresholded, 196);
	thresholded.save_image("out_threshold.bmp");

	bitmap_image eroded(width, height);
	StructureElement erodeKernel(5, StructureElement::SQUARE);
	Erode(thresholded, eroded, erodeKernel, 5);
	eroded.save_image("out_eroded.bmp");

	bitmap_image dilated(width, height);
	StructureElement dilateKernel(3, StructureElement::SQUARE);
	Dilate(eroded, dilated, dilateKernel, 1);
	dilated.save_image("out_dilated.bmp");

	ContourContainer contours;
	DetectContours(dilated, contours);
	int numCoins = contours.size();
	std::cout << "Found " << numCoins << " coins." << std::endl;

	return 0;
}


int LowImageProcessingBench()
{
	std::string imageName = "monedas_low.bmp";

	bitmap_image image(imageName);

	if (!image)
	{
		std::cout << "Error - Failed to open: input.bmp" << std::endl;
		return 1;
	}

	const unsigned int height = image.height();
	const unsigned int width  = image.width();

	bitmap_image gray(width, height);
	bitmap_image equalized(width, height);
	bitmap_image blurred(width, height);
	bitmap_image thresholded(width, height);
	bitmap_image eroded(width, height);
	bitmap_image dilated(width, height);

	StructureElement erodeKernel(5, StructureElement::SQUARE);
	StructureElement dilateKernel(3, StructureElement::SQUARE);

	ContourContainer contours;


	StartBenchmark();
	{
		ToGrayScale(image, gray);
		HistogramEqualization(gray, equalized);
		MedianBlur(equalized, blurred, 5);
		BinaryThreshold(blurred, thresholded, 196);
		Erode(thresholded, eroded, erodeKernel, 5);
		Dilate(eroded, dilated, dilateKernel, 1);
		DetectContours(dilated, contours);
	}
	EndBenchmark();

	return 0;
}


int main(int argc, char **argv)
{
#if BENCHMARKING
	int retValue = LowImageProcessingBench();
#else
	int retValue = LowImageProcessing();
#endif

	std::cout << "Operations completed." << std::endl;

	return retValue;
}

